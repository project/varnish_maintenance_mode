<?php
/**
 * @file
 * Admin pages for the Varnish maintenance mode module.
 */

/**
 * Menu callback for the maintenance mode admin page.
 * URL: admin/config/development/varnish/maintenance_mode
 */
function varnish_maintenance_mode__admin_page() {
  $form = array();

  $form['maintenance_mode_static_html'] = array(
    '#type' => 'textarea',
    '#title' => t('Static HTML'),
    '#description' => t('Enter the raw static HTML to be displayed as the maintenance page by Varnish.'),
    '#default_value' => varnish_maintenance_mode__static_html_get(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit callback for the varnish_maintenance_mode__admin_page form.
 */
function varnish_maintenance_mode__admin_page_submit($form, $form_state) {
  $html = $form_state['values']['maintenance_mode_static_html'];
  if (varnish_maintenance_mode__static_html_set($html)) {
    drupal_set_message('Your changes have been saved.');
  }
  else {
    drupal_set_message('An error occurred trying to save your changes. Try again.', 'error');
  }
}

