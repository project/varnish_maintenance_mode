<?php
/**
 * @file
 *
 * Variables:
 * - $static_html
 *   The static HTML to be displayed by Varnish.
 */
?>
#This is a Drupal maintenance-mode VCL configuration file for varnish.
backend default {
  .host = "127.0.0.1";
  .port = "80";
}

sub vcl_recv {
  # Override every request to use the error page.
  error 503 "Service Unavailable";
}

sub vcl_error {
    set obj.http.Content-Type = "text/html; charset=utf-8";
    synthetic {"<?php echo $static_html; ?>

"};
  return (deliver);
}
