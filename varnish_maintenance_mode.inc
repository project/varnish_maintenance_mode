<?php
/**
 * @file
 * Parsers to support interpretation of the messages/responses from varnish.
 */

/**
 * Interpret the current active VCL from the response of vcl.list.
 *
 * @usage
 * $current_vcl = _varnish_maintenance_mode__get_active_vcl(_varnish_terminal_run('vcl.list'));
 *
 * @param Array $vcl_list_results
 * The output returned by _varnish_terminal_run('vcl.list');
 *
 * @return Array
 * Array where the keys are the IP/port of each varnish server, and the value
 * is the name of the VCL currently in use.
 */
function _varnish_maintenance_mode__get_active_vcl($vcl_list_results) {
  $current_vcls = array();
  foreach($vcl_list_results as $server => $result) {
    // Default the value to FALSE (current VCL could not be detected).
    $current_vcls[$server] = FALSE;

    if ($result && isset($result['vcl.list']) && isset($result['vcl.list']['msg'])) {
      $varnish_response = $result['vcl.list']['msg'];

      // Parse the varnish response string.
      foreach (explode("\n", trim($varnish_response)) as $entry) {
        // The state, a numeric ID and the VCL name are whitespace-separated.
        list($state, $null, $vcl_name) = preg_split("/[\s]+/", $entry);
        if ($state == 'active') {
          $current_vcls[$server] = $vcl_name;
          break;
        }
      }
    }
  }
  return $current_vcls;
}
